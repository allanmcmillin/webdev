<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/about', function(){
//     return view('about');
// });
Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

Route::get('/user', 'UserController@view');
Route::post('/user', 'UserController@create' );

Route::post('/tweet', 'TweetController@create' );

Route::get('/', 'FrontController@view' );

Route::get('/tweets/{id}', 'TweetController@userTweets');
Route::get('/tweets/{id}/like/toggle', 'TweetController@toggleLike');

Route::get('/sandbox', function() {
    // session([
    //     'foo' => 'bar',
    //     'bat' => 'baz'
    // ]);
    $value = session('bat');
    dd($value);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
