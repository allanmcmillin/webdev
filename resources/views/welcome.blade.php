@extends('layout')
@section('content')

        <div class="container bg-primary text-light">
            <header class="">
                <div class="header-image">
                    <img src="./img/{{rand(1,111)}}.jpg" alt="">
                </div>
            </header>
            <h1>Twitter</h1>
            <form action="/tweet" method="post">
                <?php echo csrf_field()?>
                <textarea type="text" name="newContent" rows='3' cols='80'
                    placeholder="New Content"
                    class="{{ $errors->has('newContent') ? 'alert-danger': '' }}"
                    >{{ old('newContent') }}</textarea>
                <br>
                <input type="submit" name="" value="Create Tweet">
            </form>
            <h2>Users</h2>

            <br>
            <ol>
                @foreach ($users as $user)
                    <li>
                        @include('partials.user')
                    </li>
                @endforeach
            </ol>
            <h2>Tweets</h2>
                <ul>
                     @foreach ($tweets as $tweet)
                        <li>
                            @include('partials.tweet')
                        </li>
                    @endforeach
                </ul>
        </div>

    </body>
</html>
@endsection
