@extends('layout')

@section('content')

    <h1>New User Form</h1>

    <form method="post">
        <?php echo csrf_field()?>
        <input type="text" name="name"
            value="<?php echo old('name') ?>"
            placeholder="Name"
            class="<?php echo $errors->has('name') ? 'alert-danger': '' ?>"
        >
        <br>
        <input type="text" name="handle"
            value="<?php echo old('handle') ?>"
            placeholder="Handle"
            class="<?php echo $errors->has('handle') ? 'alert-danger': '' ?>"
        >
        <br>
        <input type="text" name="email"
            value="<?php echo old('email') ?>"
            placeholder="Email"
            class="<?php echo $errors->has('email') ? 'alert-danger': '' ?>"
        >
        <br>
        <input type="text" name="password"
            value="<?php echo old('password') ?>"
            placeholder="Password"
            class="<?php echo $errors->has('password') ? 'alert-danger': '' ?>"
        >
        <br>
        <input type="submit" name="" value="Submit">
    </form>

@endsection
