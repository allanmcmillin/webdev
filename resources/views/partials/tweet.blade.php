<a href="/tweets/{{$tweet->user->id}}" style="Color: white;">
        <img src="./img/{{$tweet->user->id}}.jpg" alt="" height="60px" width="60px">
        <h5>
            {{$tweet->user->handle}}
        </h5>
        {{$tweet->content}}
        <br>
    </a>
    Likes: {{count($tweet->likes)}}
    <br>
    @if ($tweet->isLikedByCurrentUser())
        <a href="/tweets/{{ $tweet->id }}/like/toggle" style="color:white;">Unlike</a>
    @else
        <a href="/tweets/{{ $tweet->id }}/like/toggle" style="color:white;">Like</a>
    @endif

    <ul>

    @foreach ($comments as $comment):
        <li>
            Comment: {{$comment->content}}
            <br>
            User:    {{$comment->users}}
        </li>
    @endforeach

</ul>
