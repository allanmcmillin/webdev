<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SAIT Winter 2018</title>
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        @if (Auth::check())
                Hello, {{Auth::user()->name}}
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @else
                <a href="/login">Login</a>
                <a href="/register">Register</a>
                <a href="/">Home</a>
            @endif


        <?php if($message = session('message')): ?>
                <div class="alert alert-success">
                    <?php echo $message ?>
                </div>
        <?php endif ?>
        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error): ?>
                        <li>
                            <?php echo $error ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif ?>
        <a href="/" style="text-decoration:none; color:black;">
            <h1 style="font-size: 5em; text-align: center;">Superkeeners are us</h1>
        </a>
        @yield('content')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" scr="/js/app.js">

        </script>
    </body>
</html>
