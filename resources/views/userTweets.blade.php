@extends('layout')

@section('content')
    <div class="container">


    <h1>{{ $user->name }}</h1>
    <img src="../img/{{$user->id}}.jpg" style="max-width: 80%;" >
    <ul>
        @foreach ($tweets as $tweet)
            <li>{{ $tweet->content }}</li>
        @endforeach
    </ul>
    </div>
@endsection
