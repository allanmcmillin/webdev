<?php

use Illuminate\Database\Seeder;
use App\Models\Tweet;
use App\Models\Comment;
use Faker\Factory;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $users = new User();
        $users->name = $faker->name;
        $users->handle = $faker->word;
        $users->email = $faker->email;
        $users->password = $faker->unique()->password;
        $users->image = "";
        $users->save();

        // for ($i=0; $i< rand(1,10);$i++)
        // {
            $tweet = new Tweet();
            $tweet->content = $faker->sentence;
            $tweet->user_id = $users->id;
            $tweet->save();
            $users->likes()->attach($tweet);
        //
        //     // for ($j=0;$j<rand(1,4);$j++){
        //     //     $comment = new Comment();
        //     //     $comment->content = $faker->bs;
        //     //     $comment->user_id = $users->id;
        //     //     $comment->tweet_id = $tweet->id;
        //     //     $comment->save();
        //     // }
        // }
    }
}
