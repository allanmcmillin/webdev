<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use App\User;

class TweetController extends Controller
{
    public function create(){
        if (!\Auth::check()) {
            return redirect('/login');
        }
        // form Validation
        $request = request();
        $result = $request->validate([
            'newContent' => 'required|max:255'
        ]);
        // do form processing here
        $data = request()->all();
        $tweets = new Tweet();
        $loggedInUser = $request->user();
        $tweets->user_id = $loggedInUser->id;
        $tweets->content = $data['newContent'];
        $tweets->save();
        return redirect('/')->with('message','Your Tweet was posted succuessuflly');
    }
    public function userTweets($userId)
    {
        $user = User::where('handle', $userId)
            ->orWhere('id', $userId)
            ->first();
        if(!$user){
            abort(404);
        }
        $tweets= $user->tweets;
        return view('userTweets',[
            'user' => $user,
            'tweets' => $tweets
        ]);
    }
    public function toggleLike($tweetId)
    {
        $user = request()->user();
        $tweet = Tweet::find($tweetId);
        if ($tweet->isLikedByCurrentUser()){
            $tweet->likes()->detach($user);
        } else {
            $tweet->likes()->attach($user);
        }
        return back()->with('message','You sucessfully liked a tweet');
    }
}
