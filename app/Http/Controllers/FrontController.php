<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tweet;
use App\Models\Comment;
use App\User;

class FrontController extends Controller
{
    public function view() {
        // Loading the Tweets from the database
        $tweets = Tweet::all();
        // Loading Users from database
        $users = User::all();
        // Loading comments
        $comments = Comment::all();

        $data = [
            'users' => $users,
            'tweets' => $tweets,
            'comments' => $comments
        ];
        return view('welcome', $data);
    }
}
